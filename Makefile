
TARGET = tt 
OBJ =  tools.o rtree.o urtree.o
CFLAGS = -Wall -c 
CC = g++ 
LFLAGS =  -Wall 

all: tt

rtree.o : rtree.h rtree.cpp
urtree.o : urtree.h urtree.cpp

%.o : %.cpp
	$(CC) $(CFLAGS) -o $@ $<

tt : $(OBJ) tt.o 
	$(CC) $(LFLAGS)  -o  $@ $(OBJ) $@.o

clean :
	rm -f *.o $(TARGET) *.old *~ x *.log



