# New version of smpnniheur.sh

function Usage()
{
    cat <<EOF

Find optimal species tree. A prototype heuristic based on NNI species tree rearangements. 

Result in directory defined by GENEFILE_OMEGA_MU_DIR where DIR is given by -d, OMEGA in -w, MU in -m
or in given dir if -D option is used.

Sorted list of computed best species trees in: cost.smp.txt
To get the optimal one use: 
head -1 cost.smp.txt 

Remember to set PATH system variable to urec and tt.

Usage: $0 
 Computing:
  -d working dir suffix  (-d or -D required)
  -D working dir exact (-d or -D required)
  -G gene trees file (required)
  -S initial species trees file (required), if -r is defined this tree is used only for the setting the order of species
  -r NUM - start from NUM random species trees 
  -c - continue computations (-G, -d, -S required)
  -R - one randomized candidate
  -t STR - ttoptions
  -e NUM - terminate when no change during last NUM loops
  -w RNUM - omega parameter (see tt)
  -m NUM - mu parameter (see tt)
  -k NUMBER - max number of NNI operations (def. 1)
  -e STOP - stop condition
 Other:
  -v DIRS_COMMA_SEP - show statistics for given dirs
  
Examples:
> Start from 10 random species trees (-r 10) 
smpnniheur.sh -G g3genetrees.txt -S g3phylogeny.txt -r 10 -d my1 -w0.2 -m5 

Result in directory g3genetrees_0.2_5_my1.

> Start from initial species trees (-r 10) 
smpnniheur.sh -G g3genetrees.txt -S candidates.txt -d my2 -w0.1 -m8 
 
Result in directory g3genetrees_0.1_8_my2.

EOF
	
} 

CND=cand.smp.txt
RCND=ccand.smp.txt
CCOMP=cost.smp.txt
OPS=1

set -- `getopt XG:S:r:D:d:vcR:t:m:w:k:e: $*`

while [ "$1" != -- ]
do
    case $1 in
	-S) SF=$2; shift;;
	-G) GF=$2; shift;;
	-r) RND=1; NUMT=$2; shift;;
	-R) RNDCAND=1; RNDCANDNUMT=$2; shift;;
	-c) CONT=1;;
	-d) DIR=$2; shift;;
	-D) EDIR=$2; shift;;
	-v) LIST=1;;
	-w) TTOPTIONS="$TTOPTIONS -w$2"; W=$2; shift;;		
	-m) TTOPTIONS="$TTOPTIONS -m$2"; M=$2; shift;;		
	-e) STOP=$2; shift;;
	-k) OPS=$2; shift;;
	-X) SMP=1;;
    esac
    shift   # next flag
done
shift   # skip --

if [ "$LIST" ]; then 
    while true; do
	for i in $*
	do
	    echo $i `head -1 $i/$CCOMP` " " `cat $i/stat.txt`
	done | sort 
	sleep 3
	echo
    done
    exit 
fi

if [ "$EDIR" ]
then
    DIR=$EDIR
else
  if ! [ "$DIR" ]
  then
      echo "Directory suffix (-d) expected"
      Usage
      exit -1
  else
      DIR=`basename $GF .txt`_$W"_"$M"_"$DIR
  fi
fi

[ -d $DIR ] || mkdir $DIR

PRC=proc.smp.txt

t=`head -1 $SF`

for i in $GF $SF
do
    if ! [ -f $i ]
    then 
	echo Input file $i does not exits
	exit -3
    fi
done

cd $DIR
LOOPN=0
Best=
NoChangeCnt=0

MAXDIV=400

if [ "$SMP" ]
then
    cd -
    # divide in case of large file

    cnt=$MAXDIV
    dest=$DIR/f
    rm -f $dest
    rm -f $DIR/cost.smp.txt
    rm -f $DIR/best
    while read a; do
	if [ "$cnt" == 0 ]
	    then
	        if ! tt -G $GF -S $dest -b $TTOPTIONS -k $OPS >> $DIR/cost.smp.txt
		then
		    echo Problem with tt
		    exit -5
		fi

		rm -f $dest
		cnt=$MAXDIV
		echo -n "."
	    fi
	echo "$a" >> $dest
	(( cnt = cnt - 1 ))
    done < $SF
 
    if ! tt -G $GF -S $dest -b $TTOPTIONS -k $OPS | sort -k1 -n >> $DIR/cost.smp.txt
    then
	echo Problem with tt
	exit -5
    fi
    rm $dest
    sort -k1 -n $DIR/cost.smp.txt | head -1 | cut -f1 -d' ' > $DIR/best
    cd -
    exit 0
fi

if [ "$CONT" ]
then
    echo "Continue from given set of candidates"
    set `cat stat.txt | tr "." " " `
    LOOPN=$1    
else
    
    #for i in *.smp.txt; do mv $i _"$i"; done
    #touch cand.smp.txt
   
    if [ "$RND" ]
    then
	echo "Start from $NUMT random trees"
	if ! urec -u -E0 -l $NUMT -r"$t" -p > $CND 
	    then
	      echo "Problem with urec..."
	      exit -1
	    fi
	cat $DIR/$CND
    else
	echo "Start from given species tree(s)"
	cd - >/dev/null
	cp $SF $DIR/$CND 
	cd - >/dev/null
	SKIPRAND=1
	touch $PRC
    fi
fi


# in DIR
while true
do
    (( LOOPN++ ))
    echo "$LOOPN.$NoChangeCnt" > stat.txt
    echo Loop $LOOPN $NoChangeCnt $Best

    if ! [ "$SKIPRAND" ] 
    then 
	if [ "$RNDCAND" ]
	then
	    cand=( $( < $CND ) )
	    #echo $cand
	    rm $CND
	    for i in `seq $RNDCANDNUMT`
	    do
		(( ind=RANDOM % ${#cand[@]} ))
		echo ${cand[$ind]} >> $CND
	    done
	fi
    else
	SKIPRAND=
    fi
    echo Number of candidates: `wc -l $CND`
#    cat $CND
#    echo "=====" tt -G ../$GF -S $CND -b $TTOPTIONS -k $OPS
#    cat $CND
#    echo "==cat end cnd"
#    echo "==tt run==="
    cd - >/dev/null
    if ! tt -G $GF -S $DIR/$CND -b $TTOPTIONS -k $OPS > $DIR/cur.smp.txt
    then
	echo Problem with tt
	exit -5
    fi
    cd - >/dev/null
#    echo "==tt res=="
#    cat cur.smp.txt 
#    echo "==tt cat end=="

    cat cur.smp.txt >> $CCOMP     
    sort -u -k1 -n $CCOMP  > a
    mv a $CCOMP
    cut -f3 -d' ' $CCOMP > tr.$CCOMP
    
    FST=1
    cat $CCOMP | while read a b x c ; do 
	if [ $FST ] 
	    then
	    NBest=$a
	    FST=
	    echo $a > best	    
	    echo WH $a $b $c	    
	else
	    echo -n "."
	fi
	if ! grep -qs "$c" $PRC
	then
	    if ! urec -l 0 -r "$t" -Ms -N -s "$c" | while read x; do 
		    echo $a $x ; done >> $RCND
	    then
		exit -1
	    fi
    	    echo "$c" >> $PRC
	    echo 
	    if [ "$NBest" != "$Best" ];
	    then 
		echo NEW cost: $NBest
	    fi
	    echo New nni: "$a $c"
	    break
	fi
    done 
    NBest=`cat best`
    if [ "$Best" == "$NBest" ]
    then
	if [ $NoChangeCnt = "$STOP" ]
	then
	    echo "No changes during last $STOP loops"
	    echo "Best cost: $Best"
	    echo "$LOOPN.$NoChangeCnt.$Best" > stat.txt
	    break
	fi
	(( NoChangeCnt++ ))
    else
	NoChangeCnt=0
	Best=$NBest
    fi
   
    if ! sort -k1 -n $RCND | while read a c ; do 
	    if ! grep -qs "$c" tr.$CCOMP
	    then
	    # new candidate
		echo $a $c
	    fi
	done > tmp.txt
    then 
	exit -1;
    fi
    mv tmp.txt $RCND
    echo "First 3 candidates: (src_nni_ngh_cost tree)"
    head -3 $RCND
    head -10 $RCND | cut -f2 -d" " > $CND

done

