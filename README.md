
TT is a tool written in C++ to perform error correction using unrooted reconciliation approach. Source files are partially based on urec project.

To compile from sources just type: "make".

Execute "tt" to get help and more usage examples.

### Cite

Algorithms: simultaneous error-correction and rooting for gene tree reconciliation and the gene duplication problem
Górecki, Eulenstein, BMC bioinformatics 13 (10), 1-11

A linear time algorithm for error-corrected reconciliation of unrooted gene trees
Górecki, Eulenstein, International Symposium on Bioinformatics Research and Applications 2011, 148-159

### Usage

    ./tt [options] ...

### Typical usage examples

Print detailed variants (-Rf) for correction of at most 2 errors (-k2) and sort the results.

```
> tt  -G gt.txt  -S st.txt -b -k2 -Rf | sort -k1 -n
DL D L Errors(NNIs) RootedGeneTree
4 1 3 2 ((((a:7,(h:8,k:9):1):2,s:5):3,e:6):11,c:11)
4 1 3 2 (((a:7,((h:8,k:9):1,s:5):3):2,e:6):11,c:11)
4 1 3 2 (((a:7,s:5):3,(e:6,(h:8,k:9):1):2):11,c:11)
4 1 3 2 (((h:8,k:9):1,(e:6,(a:7,s:5):2):3):11,c:11)
5 1 4 1 (((e:6,(a:7,(h:8,k:9):1):2):3,s:5):11,c:11)
5 1 4 1 ((((h:8,k:9):1,s:5):3,(a:7,e:6):2):11,c:11)
5 1 4 2 (((a:7,(h:8,k:9):1):2,(e:6,s:5):3):11,c:11)
5 1 4 2 (((e:6,(a:7,(h:8,k:9):1):2):3,s:5):11,c:11)
5 1 4 2 ((((h:8,k:9):1,s:5):3,(a:7,e:6):2):11,c:11)
...
```

### Tree format

TT reads trees using newick notation with branch lenghts. Trees must be binary. Note that all gene trees are unrooted, while the species tree must be rooted.

```
> cat gt.txt
(c:11,((a:7,e:6):2,(h:8,k:9):1):3,s:5)

> cat st.txt
(c,(((a,s),(h,k)),e))
```
 

### Reading trees: a string, a file or a stdin.

 `-s STR`: species trees from a string

 `-g STR`: unrooted gene trees from a string

 `-S FILE`: similar to -s, but from a file

 `-G FILE`: similar to -g, but from a file

 `-p`: print gene tree(s)

 `-P`: print species tree(s)
```

> ./urec  -g "((a1:2,a2:3):1,a3:7,a4:2); (a:2,a:3); (a:2,(a:1,b:3):1)" -p
((a3:7,a4:2):1,a1:2,a2:3)
(a:2,a:3)
(a:3,a:1,b:3)
```

### Old versions of TT (binaries only)

http://bioputer.mimuw.edu.pl/~gorecki/ec/
